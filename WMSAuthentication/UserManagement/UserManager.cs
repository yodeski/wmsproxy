﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMSAuthentication.UserManagement.DBContext;
using WMSAuthentication.UserManagement.Entities;

namespace WMSAuthentication.UserManagement
{

    public class UserManager : IUserManager

    {
        public static DbContextOptionsBuilder<UserDBContext> optionsBuilder = null;

        public UserManager(string connectionString)
        {
            optionsBuilder = new DbContextOptionsBuilder<UserDBContext>();
            optionsBuilder.UseNpgsql(connectionString);
        }

        public async Task<bool> CreateUser(string username, string email, string fullname, string salt, string hash)
        {
            using (var userContext = new UserDBContext(optionsBuilder.Options))
            {
                var wmuser = new WMSUser();
                wmuser.username = username;
                wmuser.useremail = email;
                wmuser.userfullname = fullname;
                wmuser.salt = salt;
                wmuser.hashedpassword = hash;
                userContext.Users.Add(wmuser);
                await userContext.SaveChangesAsync();
            }
            return true;
        }

        public WMSUser GetUser(string username)
        {
            using (var userContext = new UserDBContext(optionsBuilder.Options))
            {
                return userContext.Users.Where(u => u.username == username)?.FirstOrDefault();
            }
        }

        public List<WMSMaps> GetUserMaps(string username)
        {
            using (var userContext = new UserDBContext(optionsBuilder.Options))
            {

                return userContext.Maps.Where(u => u.username == username).ToList();
            }
        }
    }
}
