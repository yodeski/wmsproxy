﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WMSAuthentication.UserManagement.Entities;

namespace WMSAuthentication.UserManagement
{
    public interface IUserManager
    {
        WMSUser GetUser(string username);
        Task<bool> CreateUser(string username, string email, string fullname, string salt, string hash);
        List<WMSMaps> GetUserMaps(string username);
    }
}