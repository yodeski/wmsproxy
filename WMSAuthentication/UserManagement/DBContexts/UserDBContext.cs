﻿using Microsoft.EntityFrameworkCore;
using WMSAuthentication.UserManagement.Entities;
using WMSAuthentication.Utils;

namespace WMSAuthentication.UserManagement.DBContext
{
    public class UserDBContext: DbContext
    {

        private static UserDBContext _context;
        public UserDBContext(DbContextOptions<UserDBContext> options) : base(options) {
            _context = this;
        }

        public static UserDBContext GetContext() {
            return _context;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            //quick and dirty takes care of my entities not all scenarios
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                var currentTableName = modelBuilder.Entity(entity.Name).Metadata.Relational().TableName;
                modelBuilder.Entity(entity.Name).ToTable(currentTableName.ToLower());
            }

            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Entity<WMSUser>().HasKey(p => new { p.username });
            modelBuilder.Entity<WMSUser>().Property(b => b.creationdate).HasDefaultValueSql("CURRENT_DATE");
            modelBuilder.Entity<WMSUser>().HasMany(m => m.maps).WithOne();
            modelBuilder.Entity<WMSMaps>().HasKey(m => m.id);

            var salt = hashHelper.getSalt();
            modelBuilder.Entity<WMSUser>().HasData(new WMSUser {
                username="yodeski",
                useremail="yodeski@gmail.com",
                userfullname="Yodeski Rodriguez Alvarez",
                salt = salt,
                hashedpassword=hashHelper.getHash("lolo"+salt)
            });

           
        }

        public DbSet<WMSUser> Users { get; set; }
        public DbSet<WMSMaps> Maps { get; set; }

    }
}
