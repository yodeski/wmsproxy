﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace WMSAuthentication.UserManagement.Entities
{
    public class WMSMaps
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long id { get; set; }
        public string mapname { get; set; }
        public string mappath { get; set; }
        public string username { get; set; }

    }
}
