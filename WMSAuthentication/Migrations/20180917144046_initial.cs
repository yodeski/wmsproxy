﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WMSAuthentication.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "users",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    username = table.Column<string>(nullable: false),
                    useremail = table.Column<string>(nullable: false),
                    userfullname = table.Column<string>(nullable: true),
                    hashedpassword = table.Column<string>(nullable: true),
                    salt = table.Column<string>(nullable: true),
                    creationdate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_DATE")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.username);
                });

            migrationBuilder.CreateTable(
                name: "maps",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    mapname = table.Column<string>(nullable: true),
                    mappath = table.Column<string>(nullable: true),
                    username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_maps", x => x.id);
                    table.ForeignKey(
                        name: "FK_maps_users_username",
                        column: x => x.username,
                        principalSchema: "public",
                        principalTable: "users",
                        principalColumn: "username",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "users",
                columns: new[] { "username", "creationdate", "hashedpassword", "id", "salt", "useremail", "userfullname" },
                values: new object[] { "yodeski", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "f3479376e3fa84ac34f2d830a023f4e48b70145ff19158ada8cb9a5c81fcb74c", 0L, "42b4180252d56d47eabd742d729be8e6", "yodeski@gmail.com", "Yodeski Rodriguez Alvarez" });

            migrationBuilder.CreateIndex(
                name: "IX_maps_username",
                schema: "public",
                table: "maps",
                column: "username");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "maps",
                schema: "public");

            migrationBuilder.DropTable(
                name: "users",
                schema: "public");
        }
    }
}
