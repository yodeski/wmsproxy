﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using WMSAuthentication.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WMSAuthentication.UserManagement;
using WMSAuthentication.WMSProxy;

namespace WMSAuthentication
{
    public class ReverseProxyHandler
    {
        
        private const int durationInSeconds = 60 * 60 * 24;

        private IHttpContextAccessor httpContextAccessor;
        private ILogger logger;
        private IConfiguration configuration;
        private IMemoryCache cache;
        private IUserManager userManager;
        public ReverseProxyHandler(IHttpContextAccessor httpContextAccessor, ILogger logger, IConfiguration configuration, IMemoryCache cache, IUserManager userManager) {
            this.httpContextAccessor = httpContextAccessor;
            this.logger = logger;
            this.configuration = configuration;
            this.cache = cache;
            this.userManager = userManager;
        }

        public WMSProxyOptions GetProxyOptions()
        {
            var proxyOptions = new WMSProxyOptions
            {
                ProxyRules = GetProxyRules(),
                Reporter = r =>
                {
                    if (r.ProxyStatus == WMSProxyStatus.Proxied)
                    {
                        logger.LogDebug($" New Url: {r.ProxiedUri.AbsoluteUri} Status: {r.HttpStatusCode}");
                    }
                },
                FollowRedirects = false
            };
            return proxyOptions;
        }


        private List<WMSRule> GetProxyRules()
        {
            var resultRules = new List<WMSRule>();
            
            resultRules.Add(new WMSRule
            {
                Matcher = uri => uri.AbsoluteUri.Contains("mapserv?") || uri.AbsoluteUri.Contains("sitawms"),
                Modifier = (msg, user) =>
                {
                    var headers = msg.Headers;
                    var authCookie = Authenticator.GetAuthCookie(msg)?.ToString();
                    var authResutl = new Authenticator().Authenticate(msg, authCookie, configuration.GetConnectionString("UsersDatabase"));
                    var uri = new UriBuilder(msg.RequestUri)
                    {
                        Port = int.Parse(configuration["ProxyToPort"]),
                    };
                    //uri.ReplaceSegment(authResutl.Username, cache, userManager);
                    msg.RequestUri = uri.Uri;
                    var clientIp = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    httpContextAccessor.HttpContext.Session.SetObjectAsJson<AuthenticationResult>("Auth-" + clientIp, authResutl);
                },
                PreProcessResponse = false,
                ResponseModifier = async (msg, ctx) =>
                {

                    var authResult = ctx.Session.GetObjectFromJson<AuthenticationResult>("Auth-" + ctx.Connection.RemoteIpAddress.ToString());
                    if (msg.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        foreach (var header in msg.Headers)
                        {
                            ctx.Response.Headers[header.Key] = header.Value.ToArray();
                        }
                            // SendAsync removes chunking from the response. 
                            // This removes the header so it doesn't expect a chunked response.
                            ctx.Response.Headers.Remove("transfer-encoding");

                        if (authResult.IsAuthenticated)
                        {
                            if (authResult.Regenerate)
                            {
                                //var guid = Guid.NewGuid().ToString("N");
                                //cache.Set(guid, authResult.Token);
                                ctx.Response.Headers.Add("set-cookie", string.Format("WMS.Auth= {0}", hashHelper.Base64Encode(authResult.Token)));
                            }
                            if (msg.Content != null)
                            {
                                    /*foreach (var contentHeader in msg.Content.Headers)
                                    {
                                        ctx.Response.Headers[contentHeader.Key] = contentHeader.Value.ToArray();
                                    }*/
                                ctx.Response.Headers[HeaderNames.Expires] = DateTime.Now.AddSeconds(durationInSeconds).ToLongDateString();
                                ctx.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                                await msg.Content.CopyToAsync(ctx.Response.Body);
                            }
                        }
                        else
                        {
                            byte[] data = Encoding.UTF8.GetBytes(authResult.Reason);

                            ctx.Response.StatusCode = authResult.ErrorCode;
                            ctx.Response.Headers.Add("StatusReason", authResult.Reason);
                            ctx.Response.ContentLength = data.Length;
                            await ctx.Response.Body.WriteAsync(data, 0, data.Length);
                        }
                    }
                    else
                    {
                        await msg.Content.CopyToAsync(ctx.Response.Body);
                    }
                }
            });
            //}
            return resultRules;
        }
    }
}
