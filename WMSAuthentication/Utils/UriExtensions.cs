﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMSAuthentication.UserManagement;

namespace WMSAuthentication.Utils
{
    public static class UriExtensions
    {
        private const string MAP_FILE_LOCATION = "/var/www/appgiswms/maps/";
        public static void ReplaceSegment(this UriBuilder uribuilder, string username, IMemoryCache cache, IUserManager userManager)
        {
            Dictionary<string,string> cachedUrls = (Dictionary<string, string>)cache.Get(CacheEntries.URLMapEntry);
            var foundMap = (cachedUrls!=null);
            if (foundMap)
            {
                GetPathAndQuery(cachedUrls, ref uribuilder);
            }
            else 
            {
                UpdateCacheUrls(username, cache, userManager, ref uribuilder);
            }
        }

        private static void GetPathAndQuery(Dictionary<string, string> cachedUrls, ref UriBuilder uribuilder)
        {
            foreach (var item in cachedUrls)
            {
                var q = item.Value.Split('?');
                if (uribuilder.Path.Contains(item.Key))
                {
                    uribuilder.Path = uribuilder.Path.Replace(item.Key, q[0]);
                    uribuilder.Query += string.Format("&{0}", q?[1]);
                    break;
                }
            }
        }
        private static void UpdateCacheUrls (string username, IMemoryCache cache, IUserManager userManager, ref UriBuilder uribuilder)
        {
            var maps = userManager.GetUserMaps(username);
            var mapurl = new Dictionary<string, string>();
            foreach (var m in maps)
            {
                mapurl.Add(m.mapname, string.Format("mapserv?map={0}{1}", MAP_FILE_LOCATION, m.mappath));
            }
            if (mapurl.Count > 0)
            {
                cache.Set(CacheEntries.URLMapEntry, mapurl);
                GetPathAndQuery(mapurl, ref uribuilder);
            }
        }
    }
}
