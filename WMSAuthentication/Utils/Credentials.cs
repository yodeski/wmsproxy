﻿namespace WMSAuthentication.Utils
{
    public enum AuthMethod
    {
        Basic,
        TokenBased
    }
    public class Credentials
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string token { get; set; }

        public AuthMethod method { get; set; }

        public Credentials()
        {
            method = AuthMethod.Basic;
        }

    }
}