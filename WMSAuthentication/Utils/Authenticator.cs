﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using WMSAuthentication.UserManagement;

namespace WMSAuthentication.Utils
{
    public class Authenticator : IAuthenticator
    {
        public AuthenticationResult Authenticate(HttpRequestMessage req, string authCookie, string cnn)
        {

            //var optionsBuilder = new DbContextOptionsBuilder<UserDBContext>();
            //optionsBuilder.UseNpgsql(cnn);
            //using (var userContext = new UserDBContext(optionsBuilder.Options))
            //{
            var userManager = new UserManager(cnn);

            bool goAuthenticate = false;
            Credentials cred = null;
            var requestToken = authCookie; // GetAuthCookie(req);
            if (!String.IsNullOrEmpty(requestToken))
            {
                var jwt = new JwtSecurityTokenHandler().ReadJwtToken(requestToken) as JwtSecurityToken;
                TimeSpan diffDate = DateTime.Now.Subtract(jwt.ValidTo);
                if (jwt != null && diffDate.Minutes < 2)
                    return new AuthenticationResult()
                    {
                        IsAuthenticated = true,
                        Username = jwt.Payload.Values.First().ToString(),
                        Token = requestToken,
                        Regenerate = false
                    };
                else
                    goAuthenticate = true;
            }
            else
            {
                goAuthenticate = true;
            }
            if (goAuthenticate)
                cred = GetCredentials(req);

            if (cred != null)
            {
                var query = userManager.GetUser(cred.userName); //userContext.Users.Where(u => u.username == cred.userName)?.FirstOrDefault();
                if (query == null)
                {
                    return new AuthenticationResult()
                    {
                        IsAuthenticated = false,
                        ErrorCode = 401,
                        Reason = "User does not exist"
                    };
                }
                else
                {
                    var salt = query.salt;
                    var hash = hashHelper.getHash(cred.password + salt);
                    if (query.hashedpassword == hash)
                    {
                        var token = hashHelper.BuildToken(cred.userName, req.Headers.Host, "wms");
                        return new AuthenticationResult()
                        {
                            IsAuthenticated = true,
                            Username = cred.userName,
                            Token = token,
                            Regenerate = true
                        };
                    }
                    else
                        return new AuthenticationResult() { IsAuthenticated = false, ErrorCode = 401, Reason = "Authentication failed. Wrong password" };
                }
            }
            else
                return new AuthenticationResult() { IsAuthenticated = false, ErrorCode = 401, Reason = "You must provide user and password." };
            //}
        }

        public Credentials GetCredentials(HttpRequestMessage req)
        {
            var authString = "";
            var username = "";
            var password = "";
            var headers = req.Headers;
            IEnumerable<string> authHeader;
            if (headers.TryGetValues("Authorization", out authHeader))
            {
                authString = authHeader.First();
            }
            
            if (authHeader != null && !string.IsNullOrEmpty(authString))
            {
                if (authString.Contains("Basic "))
                    authString = authString.Split(' ')[1];
                else authString = null;
            }

            if (!String.IsNullOrEmpty(authString))
            {
                string decodedstr = Encoding.UTF8.GetString(Convert.FromBase64String(authString));
                username = decodedstr.Split(':')[0];
                password = decodedstr.Split(':')[1];
            }
            else
            {
                var qrystr = req.RequestUri.ParseQueryString();// RequestUri.Split('&', StringSplitOptions.RemoveEmptyEntries)?.Select(s => s.Split('='));
                foreach (string key in qrystr)
                {
                    if (key.Equals("username"))
                        username = qrystr[key];
                    if (key.Equals("password"))
                        password = qrystr[key];
                    if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
                        break;
                }
            }

            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                return null;

            var cred = new Credentials();
            cred.userName = username;
            cred.password = password;

            return cred;
        }

        public static string GetAuthCookie(HttpRequestMessage req)
        {
            var headers = req.Headers;
            IEnumerable<string> authHeader;
            string authString = null;
            if (headers.TryGetValues("Cookie", out authHeader))
            {
                authString = authHeader.First();
            }

            if (authHeader != null && !string.IsNullOrEmpty(authString))
            {
                authString = (authString.Contains("WMS.Auth=")) ? authString.Split("WMS.Auth=")[1].Split(';')[0] : string.Empty;
                return hashHelper.Base64Decode(authString);
            }
            return string.Empty;
        }


    }
}
