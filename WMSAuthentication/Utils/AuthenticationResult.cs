﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WMSAuthentication.Utils
{
    public class AuthenticationResult
    {
        public bool IsAuthenticated { get; set; }
        public int ErrorCode { get; set; }
        public string Reason { get; set; }

        public string Username { get; set; }
        public string Token { get; set; }

        public bool Regenerate { get; set; }
    }
}
