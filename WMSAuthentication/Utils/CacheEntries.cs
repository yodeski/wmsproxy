﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMSAuthentication.Utils
{
    public static class CacheEntries
    {
        public static string ProxyRules { get { return "_Proxy_Rules"; } }
        public static string URLMapEntry { get { return "_URL_Map"; } }
        //public static string CallbackMessage { get { return "_CallbackMessage"; } }
        //public static string Parent { get { return "_Parent"; } }
        //public static string Child { get { return "_Child"; } }
    }
}
