﻿using System;
using System.Net.Http;

namespace WMSAuthentication.Utils
{
    public interface IAuthenticator
    {
        AuthenticationResult Authenticate(HttpRequestMessage req, string autCookie, string cnn);
        Credentials GetCredentials(HttpRequestMessage req);
    }
}
