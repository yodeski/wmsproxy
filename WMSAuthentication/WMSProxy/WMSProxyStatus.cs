namespace WMSAuthentication.WMSProxy {
    public enum WMSProxyStatus {
        NotProxied,
        Proxied,
        NotAuthenticated
    }
}