using System;
using Microsoft.AspNetCore.Http;

namespace WMSAuthentication.WMSProxy {
    public class WMSProxiedResultFactory {
        private WMSProxiedResult _result;
        private DateTime _start;
        public WMSProxiedResultFactory(Uri originalUri) {
            _result = new WMSProxiedResult {
                OriginalUri = originalUri
            };
            _start = DateTime.Now;
        }

        public WMSProxiedResult Proxied(Uri proxiedUri, int statusCode) {
            Finish(WMSProxyStatus.Proxied);
            _result.ProxiedUri = proxiedUri;
            _result.HttpStatusCode = statusCode;
            return _result;
        }

        public WMSProxiedResult NotProxied(int statusCode) {
            Finish(WMSProxyStatus.NotProxied);
            _result.HttpStatusCode = statusCode;
            return _result;
        }

        public WMSProxiedResult NotAuthenticated() {
            Finish(WMSProxyStatus.NotAuthenticated);
            _result.HttpStatusCode = StatusCodes.Status401Unauthorized;
            return _result;
        }

        private WMSProxiedResult Finish(WMSProxyStatus proxyStatus) {
            _result.ProxyStatus = proxyStatus;
            _result.Elapsed = DateTime.Now - _start;
            return _result;
        }
    }
}