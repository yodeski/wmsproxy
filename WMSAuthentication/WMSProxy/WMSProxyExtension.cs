﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace WMSAuthentication.WMSProxy {
    public static class WMSProxyExtension {

        /// <summary>
        /// Sends request to remote server as specified in options
        /// </summary>
        /// <param name="app"></param>
        /// <param name="proxyOptions">Options and rules for proxy actions</param>
        /// <returns></returns>
        public static IApplicationBuilder UseProxy(this IApplicationBuilder app, WMSProxyOptions proxyOptions) {
            return app.UseMiddleware<WMSProxyMiddleware>(Options.Create(proxyOptions));
        }

        public static IApplicationBuilder UseProxy(this IApplicationBuilder app, List<WMSRule> rules, Action<WMSProxiedResult> reporter = null) {
            return app.UseMiddleware<WMSProxyMiddleware>(Options.Create(new WMSProxyOptions(rules, reporter)));
        }
    }
}
