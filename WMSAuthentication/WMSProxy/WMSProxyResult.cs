using System;
using System.Net;

namespace WMSAuthentication.WMSProxy {
    public class WMSProxiedResult {
        public WMSProxyStatus ProxyStatus { get; set; }
        public int HttpStatusCode { get; set; }
        public Uri OriginalUri { get; set; }
        public Uri ProxiedUri { get; set; }
        public TimeSpan Elapsed { get; set; }
    }
}