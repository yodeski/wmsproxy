using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace WMSAuthentication.WMSProxy {
    public class WMSProxyOptions {
        public List<WMSRule> ProxyRules { get; set; } = new List<WMSRule>();
        public HttpMessageHandler BackChannelMessageHandler { get; set; }
        public Action<WMSProxiedResult> Reporter { get; set; } = result => { };

        public bool FollowRedirects { get; set; } = true;
        public bool AddForwardedHeader { get; set; } = false;

        public WMSProxyOptions() {}

        public WMSProxyOptions(List<WMSRule> rules, Action<WMSProxiedResult> reporter = null) {
            ProxyRules = rules;
            if (reporter != null) {
                Reporter = reporter;
            }
        }

        public void AddProxyRule(WMSRule rule) {
            ProxyRules.Add(rule);
        }
    }
}